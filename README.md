### quattro-fluentd-k8s container

### Customizations

The k8s-cluster logging is shipped to EFK with fluentd with this setup:
[CoreOS-fluentd](https://coreos.com/tectonic/docs/latest/admin/logging.html)

The fluentd-container used by CoreOS is version 0.12. This version does not have support for creating timestamps with milliseconds and using placeholders in index-names. Changes:

1. upgraded docker-image from 0.12 to 1.1.2 ```FROM fluent/fluentd:v1.1.2-debian```
2. installed the necessary plugins  
    * ```&& gem install fluent-plugin-kubernetes_metadata_filter \
    && gem install fluent-plugin-rewrite-tag-filter \
    && gem install fluent-plugin-prometheus \
    && gem install fluent-plugin-systemd -v 0.1.0 \```
